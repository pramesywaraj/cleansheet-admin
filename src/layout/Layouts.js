import React from 'react'
import { useHistory } from 'react-router-dom'
import clsx from 'clsx'
import PropTypes from 'prop-types'

import { useStore } from 'context/store'

import { makeStyles, useTheme } from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
// import CssBaseline from '@material-ui/core/CssBaseline'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import List from '@material-ui/core/List'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Button from '@material-ui/core/Button'

import Home from '@material-ui/icons/Home'
import Contacts from '@material-ui/icons/Contacts'
import SettingsApplicationsRoundedIcon from '@material-ui/icons/SettingsApplicationsRounded'
import ShoppingBasketRoundedIcon from '@material-ui/icons/ShoppingBasketRounded'
import SupervisedUserCircleRoundedIcon from '@material-ui/icons/SupervisedUserCircleRounded'
import AssignmentIndRoundedIcon from '@material-ui/icons/AssignmentIndRounded'
import PersonPinRoundedIcon from '@material-ui/icons/PersonPinRounded'

import {
	REMOVE_USER,
	LOGOUT_SUCCESS,
	DRAWER_OPEN,
	DRAWER_CLOSE,
} from 'constants/command'

const drawerWidth = 240

const MenuItems = [
	{
		name: 'Dashboard',
		icon: Home,
		url: '/admin',
	},
	{
		name: 'Akun Rekening',
		icon: Contacts,
		url: '/admin/akun',
	},
	{
		name: 'Produk',
		icon: ShoppingBasketRoundedIcon,
		url: '/admin/produk',
	},
	{
		name: 'Layanan',
		icon: SettingsApplicationsRoundedIcon,
		url: '/admin/layanan',
	},
	{
		name: 'Pendaftar Cleansheet',
		icon: AssignmentIndRoundedIcon,
		url: '/admin/pendaftar',
	},
	{
		name: 'Mitra Cleansheet',
		icon: SupervisedUserCircleRoundedIcon,
		url: '/admin/mitra',
	},
	{
		name: 'Pengguna Cleansheet',
		icon: PersonPinRoundedIcon,
		url: '/admin/pengguna',
	},
	// {
	// 	name: 'Klien Cleansheet',
	// 	icon: PersonPinRoundedIcon,
	// 	url: '/admin/klien',
	// },
]

const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex',
	},
	appBar: {
		transition: theme.transitions.create(['margin', 'width'], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen,
		}),
	},
	appBarShift: {
		width: `calc(100% - ${drawerWidth}px)`,
		marginLeft: drawerWidth,
		transition: theme.transitions.create(['margin', 'width'], {
			easing: theme.transitions.easing.easeOut,
			duration: theme.transitions.duration.enteringScreen,
		}),
	},
	menuButton: {
		marginRight: theme.spacing(2),
	},
	hide: {
		display: 'none',
	},
	drawer: {
		width: drawerWidth,
		flexShrink: 0,
	},
	drawerPaper: {
		width: drawerWidth,
	},
	drawerHeader: {
		display: 'flex',
		alignItems: 'center',
		padding: theme.spacing(0, 1),
		// necessary for content to be below app bar
		...theme.mixins.toolbar,
		justifyContent: 'flex-end',
	},
	content: {
		flexGrow: 1,
		padding: theme.spacing(3),
		transition: theme.transitions.create('margin', {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen,
		}),
		marginLeft: -drawerWidth,
	},
	contentShift: {
		transition: theme.transitions.create('margin', {
			easing: theme.transitions.easing.easeOut,
			duration: theme.transitions.duration.enteringScreen,
		}),
		marginLeft: 0,
	},
	title: {
		flexGrow: 1,
	},
}))

export default function Layouts({ children }) {
	const classes = useStyles()
	const theme = useTheme()
	const history = useHistory()

	const { state, dispatch } = useStore()

	function handleDrawerOpen() {
		dispatch({
			type: DRAWER_OPEN,
		})
	}

	function handleDrawerClose() {
		dispatch({
			type: DRAWER_CLOSE,
		})
	}

	function onClickMenuItem(url) {
		history.push(url)
	}

	function handleLogout() {
		dispatch({
			type: REMOVE_USER,
		})

		dispatch({
			type: LOGOUT_SUCCESS,
		})
		history.push('/')
	}

	const { drawer } = state

	return (
		<div className={classes.root}>
			<AppBar
				position="fixed"
				className={clsx(classes.appBar, {
					[classes.appBarShift]: drawer,
				})}
			>
				<Toolbar>
					<IconButton
						color="inherit"
						aria-label="open drawer"
						onClick={handleDrawerOpen}
						edge="start"
						className={clsx(classes.menuButton, drawer && classes.hide)}
					>
						<MenuIcon />
					</IconButton>
					<Typography variant="h6" noWrap className={classes.title}>
						Cleansheet Admin
					</Typography>
					<Button onClick={handleLogout} color="inherit">
						Keluar
					</Button>
				</Toolbar>
			</AppBar>
			<Drawer
				className={classes.drawer}
				variant="persistent"
				anchor="left"
				open={drawer}
				classes={{
					paper: classes.drawerPaper,
				}}
			>
				<div className={classes.drawerHeader}>
					<IconButton onClick={handleDrawerClose}>
						{theme.direction === 'ltr' ? (
							<ChevronLeftIcon />
						) : (
							<ChevronRightIcon />
						)}
					</IconButton>
				</div>
				<Divider />
				<List>
					{MenuItems.map((item, index) => (
						<ListItem
							onClick={() => onClickMenuItem(item.url)}
							button
							key={index}
						>
							<ListItemIcon>
								<item.icon />
							</ListItemIcon>
							<ListItemText primary={item.name} />
						</ListItem>
					))}
				</List>
			</Drawer>
			<main
				className={clsx(classes.content, {
					[classes.contentShift]: drawer,
				})}
			>
				<div className={classes.drawerHeader} />
				{children}
			</main>
		</div>
	)
}

Layouts.propTypes = {
	children: PropTypes.node.isRequired,
}
