import React from 'react'
import { Router, Switch, Route, Redirect } from 'react-router-dom'
import PropTypes from 'prop-types'

import Layouts from 'layout/Layouts'
import history from 'utils/history'
import { useStore } from 'context/store'

import Login from 'pages/Login/Login'
import Dashboard from 'pages/Dashboard/Dashboard'
import ContactAccount from 'pages/Account/ContactAccount'
// import Clients from 'pages/Clients/Clients'
import Partners from 'pages/Partners/Partners'
import Products from 'pages/Products/Products'
import ProductOrdersList from 'pages/Products/ProductOrdersList'
import ProductOrderDetail from 'pages/Products/ProductOrderDetail'
import Registrants from 'pages/Registrants/Registrants'
import Services from 'pages/Services/Services'
import ServiceOrdersList from 'pages/Services/ServiceOrdersList'
import ServiceOrderDetail from 'pages/Services/ServiceOrderDetail'
import Users from 'pages/Users/Users'

import useSnackbar from 'hooks/useSnackbar'

export const ROUTES = [
	{
		path: '/',
		key: 'LOGIN',
		exact: true,
		layout: false,
		component: Login,
	},
	{
		path: '/admin',
		key: 'ADMIN_ROUTES',
		component: CheckRouteAuth,
		routes: [
			{
				path: '/admin',
				key: 'DASHBOARD',
				exact: true,
				layout: true,
				component: Dashboard,
			},
			{
				path: '/admin/akun',
				key: 'ADMIN_ACCOUNT',
				exact: true,
				layout: true,
				component: ContactAccount,
			},
			{
				path: '/admin/mitra',
				key: 'PARTNERS',
				exact: true,
				layout: true,
				component: Partners,
			},
			{
				path: '/admin/produk',
				key: 'PRODUCTS',
				exact: true,
				layout: true,
				component: Products,
			},
			{
				path: '/admin/produk/pesanan',
				key: 'PRODUCT_ORDERS_LIST',
				exact: true,
				layout: true,
				component: ProductOrdersList,
			},
			{
				path: '/admin/produk/pesanan/:id',
				key: 'PRODUCT_ORDER_DETAIL',
				exact: true,
				layout: true,
				component: ProductOrderDetail,
			},
			{
				path: '/admin/layanan',
				key: 'SERVICES',
				exact: true,
				layout: true,
				component: Services,
			},
			{
				path: '/admin/layanan/pesanan',
				key: 'SERVICE_ORDERS_LIST',
				exact: true,
				layout: true,
				component: ServiceOrdersList,
			},
			{
				path: '/admin/produk/layanan/:id',
				key: 'SERVICE_ORDER_DETAIL',
				exact: true,
				layout: true,
				component: ServiceOrderDetail,
			},
			{
				path: '/admin/pendaftar',
				key: 'REGISTRANTS',
				exact: true,
				layout: true,
				component: Registrants,
			},

			{
				path: '/admin/pengguna',
				key: 'USER_LIST',
				exact: true,
				layout: true,
				component: Users,
			},
			// {
			// 	path: '/admin/klien',
			// 	key: 'CLIENTS',
			// 	exact: true,
			// 	layout: true,
			// 	component: Clients,
			// },
		],
	},
]

function CheckRouteAuth(props) {
	const [openSnackbar] = useSnackbar()
	const { state } = useStore()

	const { isUserLoggedIn } = state

	if (!isUserLoggedIn) {
		openSnackbar('warning', 'Anda perlu login terlebih dahulu.')
		return <Redirect to={'/'} />
	}
	return <RenderRoutes {...props} />
}

function ModifiedRoute(route) {
	const { layout } = route

	if (!layout)
		return (
			<Route
				path={route.path}
				exact={route.exact}
				render={(props) => <route.component {...props} routes={route.routes} />}
			/>
		)

	return (
		<>
			<Layouts>
				<Route
					path={route.path}
					exact={route.exact}
					render={(props) => (
						<route.component {...props} routes={route.routes} />
					)}
				/>
			</Layouts>
		</>
	)
}

export default function RenderRoutes({ routes }) {
	return (
		<Router history={history}>
			<Switch>
				{routes.map((route) => {
					return <ModifiedRoute key={route.key} {...route} />
				})}
				<Route component={() => <Redirect to="/admin" />} />
			</Switch>
		</Router>
	)
}

RenderRoutes.propTypes = {
	routes: PropTypes.arrayOf(
		PropTypes.shape({
			route: PropTypes.object,
		})
	),
}
