import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'

import { Typography, Box } from '@material-ui/core'

const styles = makeStyles({
	Label: {
		color: 'rgba(0,0,0, 0.5)',
	},
})

export default function OrderDetailField({ label, value, tagStyle, isTag }) {
	const classes = styles()

	return (
		<Box
			display="flex"
			flexDirection="column"
			width="auto"
			height="auto"
			marginY={1}
		>
			<Typography className={classes.Label} component="div" variant="body2">
				{label}
			</Typography>
			<Typography component="div" variant="body2">
				{isTag && <div style={{ ...tagStyle }}>{value}</div>}
				{!isTag && value}
			</Typography>
		</Box>
	)
}

OrderDetailField.propTypes = {
	label: PropTypes.string.isRequired,
	value: PropTypes.string,
	tagStyle: PropTypes.object,
	isTag: PropTypes.bool,
}

OrderDetailField.defaultProps = {
	tagStyle: {},
	isTag: false,
}
