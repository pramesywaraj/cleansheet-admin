import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'

import { Typography, Box } from '@material-ui/core'
import Image from 'components/Image/Image'

const styles = makeStyles({
	OrderImage: {
		width: 40,
		height: 40,
		marginRight: 10,
	},
	MarginRight10: {
		marginRight: 10,
	},
})

export default function OrderDetailItem({ image, name, amount, price }) {
	const classes = styles()

	return (
		<Box
			display="flex"
			flexDirection="row"
			width="100%"
			height="auto"
			alignItems="center"
			marginY={1}
			borderBottom="1px solid rgba(0,0,0,0.2)"
			padding={1}
		>
			<Typography
				className={classes.MarginRight10}
				component="div"
				variant="body2"
				gutterBottom
			>
				{amount && `${amount} X`}
			</Typography>
			<Image
				src={image}
				alt="Order Detail Image"
				classes={classes.OrderImage}
			/>
			<Typography variant="body2" gutterBottom>
				{name}
			</Typography>
			{price && (
				<Typography variant="body2" gutterBottom>
					{`Rp. ${price}`}
				</Typography>
			)}
		</Box>
	)
}

OrderDetailItem.propTypes = {
	image: PropTypes.node,
	name: PropTypes.string,
	amount: PropTypes.string,
	price: PropTypes.string,
}
