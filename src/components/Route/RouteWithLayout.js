import React from 'react';
import { Route } from "react-router-dom";
import PropTypes from 'prop-types'

export default function RouteWithLayout({ layout: Layout, component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={routeProp => (
        <Layout>
          <Component {...routeProp} />
        </Layout>
      )}
    />
  );
}

RouteWithLayout.propTypes = {
  component: PropTypes.any.isRequired,
  layout: PropTypes.any.isRequired,
  path: PropTypes.string
}
