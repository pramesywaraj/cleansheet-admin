import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/styles'

import Modal from './Modal'
import Box from '@material-ui/core/Box'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

const styles = makeStyles({
	FormContainer: {
		width: '30vw',
	},
	Field: {
		width: '100%',
	},
	SubmitButton: {
		width: '100%',
		marginTop: 20,
	},
	ImageInput: {
		marginTop: 10,
		marginBottom: 10,
	},
	ImageName: {
		marginLeft: 10,
	},
})

function AddClientForm({
	clientData,
	imageName,
	handleChangeValue,
	handleImageChange,
	handleSubmit,
}) {
	const { name } = clientData
	const classes = styles()

	return (
		<Box component="div" className={classes.FormContainer}>
			<TextField
				className={classes.Field}
				type="text"
				name="name"
				value={name}
				label="Nama Produk"
				onChange={handleChangeValue}
				variant="outlined"
				margin="dense"
			/>
			<Box className={classes.ImageInput} display="flex" alignItems="center">
				<Button variant="outlined" component="label">
					Pilih Gambar
					<input
						type="file"
						accept="image/*"
						style={{ display: 'none' }}
						onChange={handleImageChange}
					/>
				</Button>
				<Typography className={classes.ImageName}>
					{imageName ? imageName : 'Tidak ada gambar'}
				</Typography>
			</Box>
			<Button
				className={classes.SubmitButton}
				color="primary"
				variant="contained"
				onClick={handleSubmit}
			>
				Buat Baru
			</Button>
		</Box>
	)
}

export default function AddClientModal({
	open,
	clientData,
	imageName,
	handleClose,
	handleChangeValue,
	handleImageChange,
	handleSubmit,
}) {
	return (
		<Modal
			title="Buat Klien Baru"
			content={
				<AddClientForm
					clientData={clientData}
					imageName={imageName}
					handleChangeValue={handleChangeValue}
					handleImageChange={handleImageChange}
					handleSubmit={handleSubmit}
				/>
			}
			isOpened={open}
			handleClose={handleClose}
		/>
	)
}

AddClientModal.propTypes = {
	open: PropTypes.bool,
	handleClose: PropTypes.func,
	handleChangeValue: PropTypes.func,
	handleImageChange: PropTypes.func,
	handleSubmit: PropTypes.func,
	clientData: PropTypes.object,
	imageName: PropTypes.string,
}

AddClientForm.propTypes = {
	clientData: PropTypes.object,
	imageName: PropTypes.string,
	handleChangeValue: PropTypes.func,
	handleImageChange: PropTypes.func,
	handleSubmit: PropTypes.func,
}
