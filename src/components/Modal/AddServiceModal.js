import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/styles'

import Modal from './Modal'
import Box from '@material-ui/core/Box'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import InputLabel from '@material-ui/core/InputLabel'

const styles = makeStyles({
	FormContainer: {
		width: '30vw',
	},
	Field: {
		width: '100%',
	},
	SubmitButton: {
		width: '100%',
		marginTop: 20,
	},
	ImageInput: {
		marginTop: 10,
		marginBottom: 10,
	},
	ImageName: {
		marginLeft: 10,
	},
	CategorySelect: {
		width: '100%',
	},
})

const servicesTypeEnum = [
	{
		name: 'Sanitasi',
		value: 'SANITATION',
	},
	{
		name: 'Cuci Barang',
		value: 'WASH_ITEM',
	},
	{
		name: 'Kebersihan Lingkungan',
		value: 'ENVIRONMENT',
	},
]

function AddServiceForm({
	onEdit,
	newService,
	imageName,
	handleChangeValue,
	handleImageChange,
	handleSubmit,
}) {
	const {
		category,
		title,
		description,
		price,
		unit,
		estimation_time,
	} = newService
	const classes = styles()

	return (
		<Box component="div" className={classes.FormContainer}>
			<FormControl variant="outlined" className={classes.CategorySelect}>
				<InputLabel htmlFor="service-categories">Kategori</InputLabel>
				<Select
					native
					value={category}
					onChange={handleChangeValue}
					label="Kategori"
					inputProps={{
						name: 'category',
						id: 'service-categories',
					}}
				>
					<option aria-label="None" value="" />
					{servicesTypeEnum.map((type, index) => (
						<option key={index} value={type.value}>
							{type.name}
						</option>
					))}
				</Select>
			</FormControl>
			<TextField
				className={classes.Field}
				type="text"
				name="title"
				value={title}
				label="Nama Layanan"
				onChange={handleChangeValue}
				variant="outlined"
				margin="dense"
			/>
			<TextField
				className={classes.Field}
				type="number"
				name="price"
				value={price}
				label="Harga Produk"
				onChange={handleChangeValue}
				variant="outlined"
				margin="dense"
			/>
			<TextField
				className={classes.Field}
				type="text"
				name="unit"
				value={unit}
				label="Ukuran"
				onChange={handleChangeValue}
				variant="outlined"
				margin="dense"
			/>
			<TextField
				className={classes.Field}
				type="text"
				name="estimation_time"
				value={estimation_time}
				label="Waktu Estimasi"
				onChange={handleChangeValue}
				variant="outlined"
				margin="dense"
			/>
			<TextField
				className={classes.Field}
				type="text"
				name="description"
				value={description}
				label="Deskripsi"
				onChange={handleChangeValue}
				variant="outlined"
				multiline
				rows={4}
				margin="dense"
			/>
			<Box className={classes.ImageInput} display="flex" alignItems="center">
				<Button variant="outlined" component="label">
					Pilih Gambar
					<input
						type="file"
						accept="image/*"
						style={{ display: 'none' }}
						onChange={handleImageChange}
					/>
				</Button>
				<Typography className={classes.ImageName}>
					{imageName ? imageName : 'Tidak ada gambar'}
				</Typography>
			</Box>
			<Button
				className={classes.SubmitButton}
				color="primary"
				variant="contained"
				onClick={handleSubmit}
			>
				{onEdit ? 'Simpan' : 'Buat Baru'}
			</Button>
		</Box>
	)
}

export default function AddServiceModal({
	open,
	onEdit,
	newService,
	imageName,
	handleClose,
	handleChangeValue,
	handleImageChange,
	handleSubmit,
}) {
	return (
		<Modal
			title={onEdit ? 'Sunting Layanan' : 'Buat Layanan Baru'}
			content={
				<AddServiceForm
					onEdit={onEdit}
					newService={newService}
					imageName={imageName}
					handleChangeValue={handleChangeValue}
					handleImageChange={handleImageChange}
					handleSubmit={handleSubmit}
				/>
			}
			isOpened={open}
			handleClose={handleClose}
		/>
	)
}

AddServiceModal.propTypes = {
	open: PropTypes.bool,
	onEdit: PropTypes.bool,
	handleClose: PropTypes.func,
	handleChangeValue: PropTypes.func,
	handleImageChange: PropTypes.func,
	handleSubmit: PropTypes.func,
	newService: PropTypes.object,
	imageName: PropTypes.string,
}

AddServiceForm.propTypes = {
	onEdit: PropTypes.bool,
	newService: PropTypes.object,
	imageName: PropTypes.string,
	handleChangeValue: PropTypes.func,
	handleImageChange: PropTypes.func,
	handleSubmit: PropTypes.func,
}
