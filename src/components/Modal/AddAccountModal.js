import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/styles'

import Modal from './Modal'
import Box from '@material-ui/core/Box'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

import Image from 'components/Image/Image'

const AddAccountFormStyle = makeStyles({
	FormContainer: {
		width: '30vw',
	},
	Field: {
		width: '100%',
	},
	SubmitButton: {
		width: '100%',
		marginTop: 20,
	},
	ImageInput: {
		marginTop: 10,
		marginBottom: 10,
	},
	ImageName: {
		marginLeft: 10,
	},
	AvailableImage: {
		width: 100,
		height: 'auto',
	},
})

function AddAccountForm({
	values,
	imageName,
	handleChangeValue,
	handleAccountImageChange,
	handleSubmit,
	onEdit,
}) {
	const {
		code,
		name,
		description,
		account_number,
		account_name,
		image_url,
	} = values
	const classes = AddAccountFormStyle()

	return (
		<form onSubmit={handleSubmit} className={classes.FormContainer}>
			<TextField
				className={classes.Field}
				type="text"
				name="code"
				value={code}
				label="Kode Akun"
				onChange={handleChangeValue}
				variant="outlined"
				margin="dense"
			/>
			<TextField
				className={classes.Field}
				type="text"
				name="name"
				value={name}
				label="Nama Akun"
				onChange={handleChangeValue}
				variant="outlined"
				margin="dense"
			/>
			<TextField
				className={classes.Field}
				type="text"
				name="description"
				value={description}
				label="Deskripsi"
				onChange={handleChangeValue}
				variant="outlined"
				margin="dense"
				multiline
			/>
			<TextField
				className={classes.Field}
				type="text"
				name="account_name"
				value={account_name}
				label="Pemilik Akun"
				onChange={handleChangeValue}
				variant="outlined"
				margin="dense"
			/>
			<TextField
				className={classes.Field}
				type="text"
				name="account_number"
				value={account_number}
				onChange={handleChangeValue}
				label="Nomor Akun"
				variant="outlined"
				margin="dense"
			/>
			<Box className={classes.ImageInput} display="flex" alignItems="center">
				<Button variant="outlined" component="label">
					Pilih Gambar
					<input
						type="file"
						accept="image/*"
						style={{ display: 'none' }}
						onChange={handleAccountImageChange}
					/>
				</Button>
				<Typography className={classes.ImageName}>
					{imageName ? imageName : 'Tidak ada gambar'}
				</Typography>
			</Box>
			{onEdit && image_url && (
				<Box
					className={classes.ImageInput}
					display="flex"
					alignItems="center"
					justifyContent="center"
				>
					<Image
						src={image_url}
						alt="Account Image"
						classes={classes.AvailableImage}
					/>
				</Box>
			)}
			<Button
				className={classes.SubmitButton}
				color="primary"
				variant="contained"
				type="submit"
			>
				{onEdit ? 'Simpan' : 'Buat Baru'}
			</Button>
		</form>
	)
}

export default function AddAccountModal({
	open,
	onEdit,
	values,
	imageName,
	handleClose,
	handleChangeValue,
	handleAccountImageChange,
	handleSubmit,
}) {
	return (
		<Modal
			title={onEdit ? 'Sunting Akun' : 'Buat Akun Baru'}
			content={
				<AddAccountForm
					values={values}
					imageName={imageName}
					handleChangeValue={handleChangeValue}
					handleAccountImageChange={handleAccountImageChange}
					handleSubmit={handleSubmit}
					onEdit={onEdit}
				/>
			}
			isOpened={open}
			handleClose={handleClose}
		/>
	)
}

AddAccountModal.propTypes = {
	open: PropTypes.bool,
	onEdit: PropTypes.bool,
	handleClose: PropTypes.func,
	handleChangeValue: PropTypes.func,
	handleAccountImageChange: PropTypes.func,
	handleSubmit: PropTypes.func,
	values: PropTypes.object,
	imageName: PropTypes.string,
}

AddAccountForm.propTypes = {
	onEdit: PropTypes.bool,
	values: PropTypes.object,
	imageName: PropTypes.string,
	handleChangeValue: PropTypes.func,
	handleAccountImageChange: PropTypes.func,
	handleSubmit: PropTypes.func,
}
