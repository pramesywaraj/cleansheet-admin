import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import PropTypes from 'prop-types'

const styles = makeStyles({
	ImageContainer: {
		display: 'block',
	},
	Image: {
		objectFit: 'contain',
		display: 'block',
		width: '100%',
		height: '100%',
	},
})

export default function Image({ src, alt, classes }) {
	const style = styles()

	return (
		<div className={clsx(style.ImageContainer, classes)}>
			<img src={src} alt={alt} className={style.Image} />
		</div>
	)
}

Image.propTypes = {
	src: PropTypes.string.isRequired,
	alt: PropTypes.string.isRequired,
	classes: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
}
