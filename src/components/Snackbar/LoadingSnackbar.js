import React from 'react'
import clsx from 'clsx'
import PropTypes from 'prop-types'
import Snackbar from '@material-ui/core/Snackbar'
import SnackbarContent from '@material-ui/core/SnackbarContent'
import CircularProgress from '@material-ui/core/CircularProgress'
import { makeStyles } from '@material-ui/core/styles'

import { blue } from '@material-ui/core/colors'

import { useStore } from 'context/store'

const useStyles1 = makeStyles((theme) => ({
	info: {
		backgroundColor: blue[100],
	},
	iconVariant: {
		opacity: 0.9,
		marginRight: theme.spacing(1),
	},
	layout: {
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center',
	},
	message: {
		color: theme.palette.primary.main,
		marginLeft: 10,
	},
}))

function MySnackbarContentWrapper(props) {
	const classes = useStyles1()
	const { className, message, ...other } = props

	return (
		<SnackbarContent
			className={clsx([classes.info, className])}
			aria-describedby="client-snackbar"
			message={
				<div className={classes.layout}>
					<CircularProgress size={20} />
					<p className={classes.message}>{message}</p>
				</div>
			}
			{...other}
		/>
	)
}

MySnackbarContentWrapper.propTypes = {
	className: PropTypes.string,
	message: PropTypes.string,
}

export default function LoadingSnackbar() {
	const {
		state: {
			snackbarLoading: { isShown, message },
		},
	} = useStore()

	// eslint-disable-next-line no-unused-vars

	return (
		<Snackbar
			anchorOrigin={{
				vertical: 'top',
				horizontal: 'right',
			}}
			open={isShown}
		>
			<MySnackbarContentWrapper message={message} />
		</Snackbar>
	)
}
