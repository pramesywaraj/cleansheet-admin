import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'

import { serviceType } from 'constants/enum'
import DefaultImage from 'assets/cleansheet-logo_new.jpg'

const useStyles = makeStyles({
	card: {
		minWidth: 345,
	},

	buttonFloatRight: {
		float: 'right',
	},
	buttonDelete: {
		color: 'red',
	},
	AdditionalInfoContainer: {
		marginTop: 20,
	},
})

export default function ServiceCard({
	deleteHandler,
	editHandler,
	serviceData,
}) {
	const classes = useStyles()

	const {
		id,
		category,
		title,
		description,
		price,
		unit,
		estimation_time,
		image_url,
	} = serviceData

	return (
		<Card className={classes.card}>
			<CardActionArea>
				<CardMedia
					component="img"
					alt="Kindly refresh the page if you want to know this image :)"
					height="140"
					image={image_url ? image_url : DefaultImage}
					title="Gambar Layanan"
				/>
				<CardContent>
					<Typography gutterBottom variant="h6" component="h4">
						{title}
					</Typography>
					<Typography variant="body2" color="textPrimary" component="p">
						{serviceType[category]}
					</Typography>
					<Typography variant="body2" color="textSecondary" component="p">
						{description}
					</Typography>
					<Box
						className={classes.AdditionalInfoContainer}
						display="flex"
						flexDirection="row"
						justifyContent="space-between"
					>
						<Typography variant="body2" color="textPrimary" component="p">
							{`Rp. ${price}`}
						</Typography>
						<Typography variant="body2" color="textPrimary" component="p">
							{`Per ${unit}`}
						</Typography>
						<Typography variant="body2" color="textPrimary" component="p">
							{estimation_time}
						</Typography>
					</Box>
				</CardContent>
			</CardActionArea>
			<CardActions className={classes.buttonFloatRight}>
				<Button
					size="small"
					color="primary"
					onClick={() => editHandler(serviceData)}
				>
					Sunting Layanan
				</Button>
				<Button
					className={classes.buttonDelete}
					size="small"
					onClick={() => deleteHandler(id)}
				>
					Hapus Layanan
				</Button>
			</CardActions>
		</Card>
	)
}

ServiceCard.propTypes = {
	deleteHandler: PropTypes.func.isRequired,
	editHandler: PropTypes.func.isRequired,
	serviceData: PropTypes.object,
}
