import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'

import DefaultImage from 'assets/cleansheet-logo_new.jpg'

const useStyles = makeStyles({
	card: {
		minWidth: 345,
	},

	buttonFloatRight: {
		float: 'right',
	},

	buttonDelete: {
		color: 'red',
	},
})

export default function ProductCard({
	deleteHandler,
	editHandler,
	productData,
}) {
	const classes = useStyles()

	const { id, name, price, image_url } = productData

	return (
		<Card className={classes.card}>
			<CardActionArea>
				<CardMedia
					component="img"
					alt="Kindly refresh the page if you want to know this image :)"
					height="140"
					image={image_url ? image_url : DefaultImage}
					title="Gambar Produk"
				/>
				<CardContent>
					<Typography gutterBottom variant="h6" component="h2">
						{name}
					</Typography>
					<Typography variant="body1" color="textPrimary" component="p">
						{`Rp. ${price}`}
					</Typography>
				</CardContent>
			</CardActionArea>
			<CardActions className={classes.buttonFloatRight}>
				<Button
					size="small"
					color="primary"
					onClick={() => editHandler(productData)}
				>
					Sunting Produk
				</Button>
				<Button
					className={classes.buttonDelete}
					size="small"
					onClick={() => deleteHandler(id)}
				>
					Hapus Produk
				</Button>
			</CardActions>
		</Card>
	)
}

ProductCard.propTypes = {
	deleteHandler: PropTypes.func.isRequired,
	editHandler: PropTypes.func.isRequired,
	productData: PropTypes.object,
}
