import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

import DefaultImage from 'assets/cleansheet-logo_new.jpg'

const useStyles = makeStyles({
	card: {
		minWidth: 345,
	},

	buttonFloatRight: {
		float: 'right',
	},

	buttonDelete: {
		color: 'red',
	},
})

export default function ClientCard({ deleteHandler, editHandler, clientData }) {
	const classes = useStyles()
	const { id, name, image_url } = clientData

	return (
		<Card className={classes.card}>
			<CardActionArea>
				<CardMedia
					component="img"
					alt="Kindly refresh the page if you want to know this image :)"
					height="140"
					image={image_url ? image_url : DefaultImage}
					title="Gambar Klien"
				/>
				<CardContent>
					<Typography gutterBottom variant="h6" component="h2">
						{name}
					</Typography>
				</CardContent>
			</CardActionArea>
			<CardActions className={classes.buttonFloatRight}>
				<Button
					size="small"
					color="primary"
					onClick={() => editHandler(clientData)}
				>
					Sunting Klien
				</Button>
				<Button
					className={classes.buttonDelete}
					size="small"
					onClick={() => deleteHandler(id)}
				>
					Hapus Klien
				</Button>
			</CardActions>
		</Card>
	)
}

ClientCard.propTypes = {
	deleteHandler: PropTypes.func.isRequired,
	editHandler: PropTypes.func.isRequired,
	clientData: PropTypes.object,
}
