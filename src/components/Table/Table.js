import React from 'react'
import PropTypes from 'prop-types'

import { makeStyles } from '@material-ui/core/styles'

import Container from '@material-ui/core/Container'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableBody from '@material-ui/core/TableBody'
import TableContainer from '@material-ui/core/TableContainer'
import Paper from '@material-ui/core/Paper'
import CircularProgress from '@material-ui/core/CircularProgress'

const useStyles = makeStyles({
	table: {
		minWidth: 650,
	},
	tableContainer: {
		padding: '2% 0',
	},
	deleteButton: {
		color: '#e30926',
	},
	LoadingContainer: {
		display: 'flex',
		width: '100%',
		minHeight: '20vh',
		justifyContent: 'center',
		alignItems: 'center',
	},
})

export default function MaterialTableLayout({
	head,
	body,
	label,
	isFetching,
	isNoData,
}) {
	const classes = useStyles()

	return (
		<Container className={classes.tableContainer}>
			<TableContainer component={Paper}>
				<Table className={classes.table} aria-label={label}>
					{head && <TableHead>{head}</TableHead>}
					{!isFetching && !isNoData && <TableBody>{body}</TableBody>}
				</Table>
				{isFetching && (
					<div className={classes.LoadingContainer}>
						<CircularProgress />
					</div>
				)}
				{isNoData && !isFetching && (
					<div className={classes.LoadingContainer}>
						<p>Tidak ada data untuk ditampilkan.</p>
					</div>
				)}
			</TableContainer>
		</Container>
	)
}

MaterialTableLayout.propTypes = {
	head: PropTypes.element,
	body: PropTypes.element,
	label: PropTypes.string,
	isFetching: PropTypes.bool,
	isNoData: PropTypes.bool,
}
