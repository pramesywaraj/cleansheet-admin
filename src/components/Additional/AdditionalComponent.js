import React from 'react'

import Snackbar from 'components/Snackbar/Snackbar'
import LoadingSnackbar from 'components/Snackbar/LoadingSnackbar'

export default function AdditionalComponent() {
	return (
		<div>
			<Snackbar />
			<LoadingSnackbar />
		</div>
	)
}
