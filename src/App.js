import React, { useEffect, useState } from 'react'
import './app.scss'

import { useStore } from './context/store'
import RenderRoutes, { ROUTES } from './routes'
import Interceptors from './utils/interceptor'
import { getUserData } from './utils/auth'

import AdditionalComponent from 'components/Additional/AdditionalComponent'

import { CssBaseline } from '@material-ui/core'
import { LOGIN_SUCCESS } from 'constants/command'

function App() {
	const [isCheckingUser, setIsCheckingUser] = useState(true)
	const { dispatch } = useStore()

	useEffect(() => {
		function checkUserData() {
			const user = getUserData()

			if (!user) return setIsCheckingUser(false)

			dispatch({
				type: LOGIN_SUCCESS,
				data: {
					...user,
				},
			})

			setIsCheckingUser(false)
		}

		checkUserData()
	}, [])

	if (isCheckingUser) return <p style={{ fontSize: '16px' }}>Loading...</p>

	return (
		<>
			<CssBaseline />
			<div className="App">
				<Interceptors />
				<RenderRoutes routes={ROUTES} />
				<AdditionalComponent />
			</div>
		</>
	)
}

export default App
