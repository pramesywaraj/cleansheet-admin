/* eslint-disable no-unused-vars */
import {
	LOGIN_SUCCESS,
	LOGOUT_SUCCESS,
	STORE_USER,
	REMOVE_USER,
	TOKEN_REFRESHED,
	SET_SELECTED_PRODUCT_ORDER,
	SET_SELECTED_SERVICE_ORDER,
	SET_SELECTED_REGISTRANT,
	SET_SELECTED_PARTNER,
	SNACKBAR_OPEN,
	SNACKBAR_CLOSE,
	SNACKBAR_LOADING_OPEN,
	SNACKBAR_LOADING_CLOSE,
	DRAWER_OPEN,
	DRAWER_CLOSE,
} from 'constants/command'

import { setUserData, removeUserData } from 'utils/auth'

export const initialState = {
	snackbar: {
		isShown: false,
		message: '',
		type: 'info',
	},
	snackbarLoading: {
		isShown: false,
		message: '',
	},
	drawer: true,
	user: {},
	isUserLoggedIn: false,
	selectedProductDetail: null,
	selectedServiceDetail: null,
	selectedRegistrantDetail: null,
	selectedPartnerDetail: null,
}

export const reducers = (state, action) => {
	switch (action.type) {
		//////////// Auth
		case LOGIN_SUCCESS: {
			const { data } = action

			return {
				...state,
				user: { ...data },
				isUserLoggedIn: true,
			}
		}
		case LOGOUT_SUCCESS: {
			return {
				...state,
				user: {},
				isUserLoggedIn: false,
			}
		}
		case STORE_USER: {
			const { data } = action

			setUserData(data)

			return {
				...state,
			}
		}
		case REMOVE_USER: {
			removeUserData()

			return {
				...state,
			}
		}
		case TOKEN_REFRESHED: {
			const { user } = state

			return {
				...state,
				user: { ...action.data },
			}
		}
		//////////// Snackbar
		case SNACKBAR_OPEN: {
			const { snackbar } = state
			const {
				data: { message, type },
			} = action

			return {
				...state,
				snackbar: {
					isShown: true,
					message,
					type,
				},
			}
		}

		case SNACKBAR_CLOSE: {
			const { snackbar } = state

			return {
				...state,
				snackbar: {
					...snackbar,
					isShown: false,
				},
			}
		}

		case SNACKBAR_LOADING_OPEN: {
			const {
				data: { message },
			} = action

			return {
				...state,
				snackbarLoading: {
					isShown: true,
					message,
				},
			}
		}

		case SNACKBAR_LOADING_CLOSE: {
			const { snackbarLoading } = state

			return {
				...state,
				snackbarLoading: {
					...snackbarLoading,
					isShown: false,
				},
			}
		}
		/////////// Drawer
		case DRAWER_OPEN: {
			return {
				...state,
				drawer: true,
			}
		}
		case DRAWER_CLOSE: {
			return {
				...state,
				drawer: false,
			}
		}
		/////////// Product
		case SET_SELECTED_PRODUCT_ORDER: {
			let { selectedProductDetail } = state

			return {
				...state,
				selectedProductDetail: action.selected,
			}
		}
		case SET_SELECTED_SERVICE_ORDER: {
			let { selectedServiceDetail } = state

			return {
				...state,
				selectedServiceDetail: action.selected,
			}
		}
		case SET_SELECTED_REGISTRANT: {
			let { selectedRegistrantDetail } = state

			return {
				...state,
				selectedRegistrantDetail: action.selected,
			}
		}
		case SET_SELECTED_PARTNER: {
			let { selectedPartnerDetail } = state

			return {
				...state,
				selectedPartnerDetail: action.selected,
			}
		}
		default:
			return state
	}
}
