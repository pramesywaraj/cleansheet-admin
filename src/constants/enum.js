import { red, green, lightBlue, amber } from '@material-ui/core/colors'

export const serviceType = {
	SANITATION: 'Kebersihan',
	WASH_ITEM: 'Cuci Barang',
	ENVIRONMENT: 'Kebersihan Lingkungan',
}

export const orderStatus = {
	NEW: { text: 'Pesanan baru', color: lightBlue[600] },
	PROCESSED: { text: 'Pesanan diproses', color: amber[600] },
	PAID: { text: 'Pesanan sudah dibayar', color: green[500] },
	REJECTED: { text: 'Pesanan dibatalkan', color: red[700] },
	COMPLETED: { text: 'Pesanan selesai', color: green[800] },
}
