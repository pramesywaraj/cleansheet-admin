import React from 'react'
import { makeStyles } from '@material-ui/core/styles'

import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'
import Box from '@material-ui/core/Box'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'

import AssignmentIndRoundedIcon from '@material-ui/icons/AssignmentIndRounded'

const useStyles = makeStyles({
	table: {
		minWidth: 650,
	},
	tableContainer: {
		padding: '2% 0',
	},
	textBoxContainer: {
		margin: '1% 0',
		width: '40%',
	},
	inputContact: {
		flex: 2,
		marginRight: 10,
	},
	tableRow: {
		cursor: 'pointer',
	},
	deleteButton: {
		color: '#e30926',
	},
})

const registrantData = [
	{
		id: '1',
		name: 'Pramesywara Jembar Panalar',
		phone_number: '081231231238',
		nim: '1231234534',
		year: '2015',
		major: 'Ilmu Komputer',
		is_bidikmisi: '0',
		address: 'Perum Kertasari, Ciamis',
		reason: 'Iseng aja',
		image_url:
			'https://gravatar.com/avatar/ff81242372b1f17ed68d8d342cf059ec/?size=200&d=identicon',
	},
]

export default function Registrants() {
	const classes = useStyles()

	return (
		<Container maxWidth="lg">
			<Typography component="h1" variant="h5">
				Pendaftar Cleansheet
			</Typography>
			<Container className={classes.tableContainer}>
				<TableContainer component={Paper}>
					<Table className={classes.table} aria-label="simple table">
						<TableHead>
							<TableRow>
								<TableCell>ID Pendaftar</TableCell>
								<TableCell>Nama Pendaftar</TableCell>
								<TableCell>No Handphone</TableCell>
								<TableCell></TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{registrantData.map((registrant) => (
								<TableRow
									className={classes.tableRow}
									hover
									key={registrant.id}
								>
									<TableCell component="th" scope="row">
										{registrant.id}
									</TableCell>
									<TableCell>{registrant.name}</TableCell>
									<TableCell>{registrant.phone_number}</TableCell>
									<TableCell align="right">
										<Button variant="contained" color="primary">
											<AssignmentIndRoundedIcon />
											Detail
										</Button>
									</TableCell>
								</TableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
			</Container>
		</Container>
	)
}
