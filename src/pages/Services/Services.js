import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { useHistory } from 'react-router-dom'

import { post, fetch } from 'utils/api'
import uploadImage from 'utils/uploadImage'
import { useStore } from 'context/store'

import Container from '@material-ui/core/Container'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import CircularProgress from '@material-ui/core/CircularProgress'
import Box from '@material-ui/core/Box'
import ServiceCard from 'components/Cards/ServiceCard'
import AddServiceModal from 'components/Modal/AddServiceModal'

import AddIcon from '@material-ui/icons/Add'
import ListAltRoundedIcon from '@material-ui/icons/ListAltRounded'

import useLoading from 'hooks/useLoading'
import useModal from 'hooks/useModal'
import useSnackbar from 'hooks/useSnackbar'
import useLoadingSnackbar from 'hooks/useLoadingSnackbar'

const useStyles = makeStyles({
	buttonMarginBottom20: {
		marginBottom: '20px',
	},
	button: {
		marginRight: 10,
	},
	progress: {
		textAlign: 'center',
	},
	centerProgress: {
		display: 'inline-block',
	},
	noServices: {
		width: '100%',
		color: '#cfcfcf',
		textAlign: 'center',
	},
})

export default function Services() {
	const initNewService = {
		category: '',
		title: '',
		description: '',
		price: '',
		unit: '',
		estimation_time: '',
		image_url: '',
	}
	const [serviceForm, setServiceForm] = useState(initNewService)
	const [services, setServices] = useState([])
	const [newServiceImage, setNewServiceImage] = useState(null)
	const [isEditService, setIsEditService] = useState(false)

	const { state } = useStore()

	const [
		fetchDataLoading,
		showFetchDataLoading,
		hideFetchDataLoading,
	] = useLoading()
	const [
		isAddServiceModalOpen,
		showAddServiceModal,
		hideAddServiceModal,
	] = useModal()
	const [openSnackbar] = useSnackbar()
	const [openLoadingSnackbar, closeLoadingSnackbar] = useLoadingSnackbar()

	const authHeader = {
		Authorization: `Bearer ${state.user.access_token}`,
	}

	const classes = useStyles()
	const history = useHistory()

	useEffect(() => {
		getServices()
	}, [])

	function goToOrdersList() {
		history.push('/admin/layanan/pesanan')
	}

	async function getServices() {
		showFetchDataLoading()

		try {
			const {
				data: { services },
				errors,
				success,
			} = await fetch(
				'admin/master/service/get-all?page=1&item_per_page=1000',
				authHeader
			)

			if (errors || !success) throw errors

			setServices([...services])
		} catch (e) {
			openSnackbar('error', e.message)
		} finally {
			hideFetchDataLoading()
		}
	}

	async function postServiceForm() {
		openLoadingSnackbar('Harap tunggu...')

		let serviceData = {
			...serviceForm,
		}

		let editSuccessMsg = 'Layanan berhasil disunting.'
		let addSuccessMsg = 'Layanan berhasil dibuat.'

		let path = 'admin/master/service/create'

		if (isEditService) path = 'admin/master/service/update'

		try {
			if (newServiceImage) {
				const { url, errors } = await uploadImage(newServiceImage)

				if (errors) throw errors

				serviceData['image_url'] = url
			}

			const { errors, success } = await post(path, serviceData, authHeader)

			if (!success || errors) throw errors

			handleHideModal()

			closeLoadingSnackbar()

			openSnackbar(
				'success',
				`${isEditService ? editSuccessMsg : addSuccessMsg}`
			)

			getServices()
		} catch (e) {
			openSnackbar('error', e.message)

			closeLoadingSnackbar()
		}
	}

	async function deleteService(id) {
		openLoadingSnackbar('Menghapus layanan...')

		try {
			const { errors, success } = await post(
				`admin/master/service/delete`,
				{ id },
				authHeader
			)

			if (errors || !success) throw errors

			closeLoadingSnackbar()

			openSnackbar('success', 'Layanan berhasil dihapus')

			getServices()
		} catch (e) {
			openSnackbar('error', e.message)

			closeLoadingSnackbar()
		}
	}

	function handleHideModal() {
		hideAddServiceModal()
		setIsEditService(false)
		setNewServiceImage(null)
		setServiceForm(initNewService)
	}

	function handleFormValueChange(e) {
		const { name, value } = e.target
		setServiceForm({
			...serviceForm,
			[name]: value,
		})
	}

	function handleImageChange(e) {
		const { files } = e.target
		setNewServiceImage(files[0])
	}

	function handleDeleteService(id) {
		if (!window.confirm('Apakah Anda yakin ingin menghapus layanan ini?'))
			return

		deleteService(id)
	}

	function handleAddNewService() {
		setIsEditService(false)

		setServiceForm({
			...initNewService,
		})

		showAddServiceModal()
	}

	function handleEditService(service) {
		setServiceForm({
			...service,
		})

		setIsEditService(true)

		showAddServiceModal()
	}

	function handleSubmit(e) {
		e.preventDefault()

		for (const [key, value] of Object.entries(serviceForm)) {
			if (!value && key !== 'image_url')
				return openSnackbar(
					'warning',
					`Harap isi seluruh isian yang dibutuhkan.`
				)
		}

		if (!isEditService && !newServiceImage)
			return openSnackbar(
				'warning',
				`Harap pilih gambar untuk merepresentasikan layanan.`
			)

		postServiceForm()
	}

	return (
		<>
			<Container>
				<Box
					display="flex"
					flexDirection="row-reverse"
					className={classes.buttonMarginBottom20}
				>
					<Button
						variant="contained"
						color="primary"
						className={classes.button}
						onClick={handleAddNewService}
					>
						<AddIcon />
						Tambahkan Layanan Baru
					</Button>
					<Button
						variant="contained"
						color="primary"
						className={classes.button}
						onClick={goToOrdersList}
					>
						<ListAltRoundedIcon />
						Lihat Pesanan Layanan
					</Button>
				</Box>
				{!fetchDataLoading && services.length === 0 && (
					<Box
						component="div"
						display="flex"
						alignItems="center"
						justifyContent="center"
						minHeight="50vh"
					>
						<h3>Tidak ada Layanan yang dapat ditampilkan.</h3>
					</Box>
				)}

				{fetchDataLoading && (
					<Box
						component="div"
						display="flex"
						alignItems="center"
						justifyContent="center"
						minHeight="50vh"
					>
						<CircularProgress />
					</Box>
				)}
			</Container>
			{!fetchDataLoading && services.length > 0 && (
				<Grid container spacing={3}>
					{services.map((service, index) => (
						<Grid item xs={4} key={index}>
							<ServiceCard
								serviceData={service}
								editHandler={handleEditService}
								deleteHandler={handleDeleteService}
							/>
						</Grid>
					))}
				</Grid>
			)}
			<AddServiceModal
				onEdit={isEditService}
				open={isAddServiceModalOpen}
				handleClose={handleHideModal}
				handleChangeValue={handleFormValueChange}
				handleImageChange={handleImageChange}
				handleSubmit={handleSubmit}
				newService={serviceForm}
				imageName={newServiceImage ? newServiceImage.name : null}
			/>
		</>
	)
}
