import React, { useState, useEffect } from 'react'
// import { makeStyles } from '@material-ui/core/styles'
import { useHistory } from 'react-router-dom'

import { useStore } from 'context/store'
import { fetch } from 'utils/api'

import {
	Container,
	TableCell,
	TableRow,
	Typography,
	Button,
} from '@material-ui/core'

import MaterialTableLayout from 'components/Table/Table'

import useLoading from 'hooks/useLoading'
import useSnackbar from 'hooks/useSnackbar'

import { orderStatus } from 'constants/enum'

export default function ServiceOrdersList() {
	const [services, setServices] = useState([])

	// const classes = useStyles()
	const { state } = useStore()
	const history = useHistory()

	const [
		fetchLoading,
		showFetchDataLoading,
		hideFetchDataLoading,
	] = useLoading()
	const [openSnackbar] = useSnackbar()

	const authHeader = {
		Authorization: `Bearer ${state.user.access_token}`,
	}

	useEffect(() => {
		getServiceOrderList()
	}, [])

	async function getServiceOrderList() {
		showFetchDataLoading()

		try {
			const {
				data: { services },
				errors,
				success,
			} = await fetch(
				'admin/order/service/get-all?page=1&item_per_page=10000',
				authHeader
			)

			if (errors || !success) throw errors

			setServices([...services])
		} catch (e) {
			openSnackbar('error', e.message)
		} finally {
			hideFetchDataLoading()
		}
	}

	function goToServiceOrderDetail(id) {
		history.push(`/admin/produk/layanan/${id}`)
	}

	return (
		<Container maxWidth="lg">
			<Typography component="h1" variant="h5">
				Daftar Pesanan layanan
			</Typography>
			<MaterialTableLayout
				head={
					<TableRow>
						<TableCell>ID Pesanan</TableCell>
						<TableCell align="center">Tanggal Pemesanan</TableCell>
						<TableCell align="center">Pemesan</TableCell>
						<TableCell align="center">Status Pemesanan</TableCell>
						<TableCell align="center"></TableCell>
					</TableRow>
				}
				body={services.map((order) => (
					<TableRow hover key={order.id}>
						<TableCell component="th" scope="row">
							{order.order_ref}
						</TableCell>
						<TableCell align="center">{order.order_date}</TableCell>
						<TableCell align="center">{order.name}</TableCell>
						<TableCell align="center">
							<div
								style={{
									padding: 8,
									backgroundColor: orderStatus[order.status].color,
									color: 'white',
									textAlign: 'center',
									borderRadius: 20,
									display: 'inline-block',
								}}
							>
								{orderStatus[order.status].text}
							</div>
						</TableCell>
						<TableCell align="center">
							<Button
								variant="contained"
								color="primary"
								onClick={() => goToServiceOrderDetail(order.id)}
							>
								Detail
							</Button>
						</TableCell>
					</TableRow>
				))}
				isFetching={fetchLoading}
				label="order-list-table"
				isNoData={services.length === 0}
			/>
		</Container>
	)
}
