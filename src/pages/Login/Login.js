import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'

import { post } from 'utils/api'
import { useStore } from 'context/store'

import {
	Paper,
	Avatar,
	Button,
	TextField,
	Typography,
	Grid,
	CircularProgress,
} from '@material-ui/core'

import useLoading from 'hooks/useLoading'
import useSnackbar from 'hooks/useSnackbar'

const useStyles = makeStyles((theme) => ({
	root: {
		backgroundColor: theme.palette.background.default,
		height: 'auto',
		width: 'auto',
	},
	container: {
		minHeight: '100vh',
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center',
	},
	paper: {
		padding: '2%',
		margin: '2%',
	},
	logoContainer: {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		margin: '2% 0',
	},
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: theme.palette.primary.main,
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
	buttonProgress: {
		color: '#4caf50',
		position: 'absolute',
		top: '50%',
		left: '50%',
		marginTop: -12,
		marginLeft: -12,
	},
}))

export default function Login() {
	const [loginState, setLoginState] = useState({
		email: '',
		password: '',
	})
	const { state, dispatch } = useStore()
	const [loading, showLoading, hideLoading] = useLoading()

	// eslint-disable-next-line no-unused-vars
	const [openSnackbar, closeSnackbar] = useSnackbar()

	const styles = useStyles()
	const history = useHistory()

	useEffect(() => {
		const { isUserLoggedIn } = state
		if (isUserLoggedIn) history.push('/admin')
	}, [])

	function handleChange(e) {
		setLoginState({
			...loginState,
			[e.target.name]: e.target.value,
		})
	}

	async function submitLogin(e) {
		e.preventDefault()

		if (!loginState.email || !loginState.password)
			return openSnackbar('warning', 'Isi email atau password terlebih dahulu.')

		showLoading()

		try {
			const { data } = await post('auth/login', {
				...loginState,
			})

			if (!data.is_admin)
				return openSnackbar('warning', 'Mohon gunakan akun admin Cleansheet :)')

			dispatch({
				type: 'LOGIN_SUCCESS',
				data: {
					...data,
				},
			})

			dispatch({
				type: 'STORE_USER',
				data: {
					...data,
				},
			})

			openSnackbar('success', 'Anda berhasil login.')

			history.push('/admin')
		} catch (err) {
			openSnackbar('error', err.message)
		} finally {
			hideLoading()
		}
	}

	return (
		<div className={styles.root}>
			<Grid className={styles.container} container>
				<Paper className={styles.paper} elevation={3}>
					<div className={styles.logoContainer}>
						<Avatar className={styles.avatar}>CS</Avatar>
						<Typography component="h1" variant="h5">
							Cleansheet Admin Apps
						</Typography>
					</div>
					<form onSubmit={submitLogin} noValidate>
						<TextField
							variant="outlined"
							margin="normal"
							required
							fullWidth
							id="email"
							label="Alamat Email Admin"
							name="email"
							autoFocus
							value={loginState.email}
							onChange={handleChange}
						/>
						<TextField
							variant="outlined"
							margin="normal"
							required
							fullWidth
							name="password"
							label="Password"
							type="password"
							id="password"
							value={loginState.password}
							onChange={handleChange}
						/>
						<Button
							type="submit"
							fullWidth
							variant="contained"
							color="primary"
							className={styles.submit}
						>
							{loading ? <CircularProgress color="secondary" /> : 'Login'}
						</Button>
					</form>
				</Paper>
			</Grid>
		</div>
	)
}
