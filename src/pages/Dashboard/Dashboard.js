import React from 'react'
import { makeStyles } from '@material-ui/core/styles'

import Image from 'components/Image/Image'
import CleansheetLogo from 'assets/cleansheet-logo_new.jpg'

const styles = makeStyles({
	CleansheetLogo: {
		width: '35vw',
		height: 'auto',
	},
})

export default function Dashboard() {
	const style = styles()

	return (
		<div
			style={{
				display: 'flex',
				minHeight: '70vh',
				width: '100%',
				justifyContent: 'center',
				alignItems: 'center',
			}}
		>
			<Image
				src={CleansheetLogo}
				alt="Cleansheet Logo"
				classes={style.CleansheetLogo}
			/>
		</div>
	)
}
