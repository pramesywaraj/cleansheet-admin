import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'

import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import CircularProgress from '@material-ui/core/CircularProgress'
import Box from '@material-ui/core/Box'
import ClientCard from 'components/Cards/ClientCard'
import AddClientModal from 'components/Modal/AddClientModal'

import AddIcon from '@material-ui/icons/Add'

import useLoading from 'hooks/useLoading'
import useModal from 'hooks/useModal'

const useStyles = makeStyles({
	buttonMarginBottom20: {
		marginBottom: '20px',
	},
	button: {
		marginRight: 10,
	},
	progress: {
		textAlign: 'center',
	},
	centerProgress: {
		display: 'inline-block',
	},
	noClient: {
		width: '100%',
		color: '#cfcfcf',
		textAlign: 'center',
	},
})

const mockClientData = [
	{
		id: '1',
		name: 'Pertamina',
		image_url: '',
	},
]

export default function Clients() {
	const initNewClient = {
		name: '',
		image_url: '',
	}
	const [newClient, setNewClient] = useState(initNewClient)
	const [clients, setClients] = useState(mockClientData)
	const [newClientImage, setNewClientImage] = useState(null)
	const [isEditClient, setIsEditClient] = useState(false)

	const [clientLoading, showClientLoading, hideClientLoading] = useLoading()
	const [
		isAddClientModalOpen,
		showAddClientModal,
		hideAddClientModal,
	] = useModal()

	const classes = useStyles()

	function handleHideModal() {
		hideAddClientModal()
		setNewClient(initNewClient)
	}

	function handleFormValueChange(e) {
		const { name, value } = e.target
		setNewClient({
			...newClient,
			[name]: value,
		})
	}

	function handleImageChange(e) {
		const { files } = e.target
		setNewClientImage(files[0])
	}

	function handleDeleteClient(id) {
		alert(`Klien berhasil ${id} berhasil dihapus`)
	}

	function handleEditClient(client) {
		setIsEditClient(true)
		setNewClient(client)

		showAddClientModal()
	}

	function submitClientForm() {
		if (isEditClient) onClientEdited()

		onAddNewClient()
	}

	function onClientEdited() {
		alert('Klien berhasil disunting')
		setNewClient(initNewClient)
	}

	function onAddNewClient() {
		alert('Klien berhasil dibuat')
		setNewClient(initNewClient)
		setIsEditClient(false)
	}

	return (
		<>
			{!clientLoading ? (
				<div>
					<div className={classes.buttonMarginBottom20}>
						<Box display="flex" flexDirection="row-reverse">
							<Button
								variant="contained"
								color="primary"
								className={classes.button}
								onClick={showAddClientModal}
							>
								<AddIcon />
								Tambahkan Klien Baru
							</Button>
						</Box>
					</div>

					<Grid container spacing={3}>
						{clients.map((client, index) => (
							<Grid item xs={4} key={index}>
								<ClientCard
									clientData={client}
									editHandler={handleEditClient}
									deleteHandler={handleDeleteClient}
								/>
							</Grid>
						))}

						{/* <div className={classes.noClient}>
								<h3>Tidak ada Produk yang dapat ditampilkan.</h3>
							</div> */}
					</Grid>
				</div>
			) : (
				<div className={classes.progress}>
					<div>
						<CircularProgress />
					</div>
				</div>
			)}
			<AddClientModal
				open={isAddClientModalOpen}
				handleClose={handleHideModal}
				handleChangeValue={handleFormValueChange}
				handleImageChange={handleImageChange}
				handleSubmit={submitClientForm}
				clientData={newClient}
				imageName={newClientImage ? newClientImage.name : null}
			/>
		</>
	)
}
