import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import { useHistory, useParams } from 'react-router-dom'

import { fetch } from 'utils/api'
import { useStore } from 'context/store'

import {
	Container,
	Paper,
	Typography,
	Grid,
	Box,
	CircularProgress,
} from '@material-ui/core'

import OrderDetailItem from 'components/Item/OrderDetailItem'
import OrderDetailField from 'components/Item/OrderDetailField'

import useSnackbar from 'hooks/useSnackbar'
import useLoading from 'hooks/useLoading'

import { orderStatus } from 'constants/enum'

const useStyles = makeStyles({
	ProductOrderContainer: {
		marginTop: 20,
	},
	OrderDetailContainer: {
		minHeight: '60vh',
		padding: '2.5%',
	},
	OrderedProducts: {
		display: 'flex',
		flexDirection: 'column',
		padding: 15,
		textAlign: 'center',
		height: 'auto',
		minHeight: 300,
	},
})

function OrderDetailFields({ detail }) {
	const {
		name,
		email,
		phone_number,
		address,
		city,
		postal_code,
		order_date,
		delivery_date,
		status,
		total,
		notes,
	} = detail

	return (
		<Box height="100%">
			<OrderDetailField
				label="Status Pesanan"
				value={orderStatus[status].text}
				tagStyle={{
					padding: 8,
					backgroundColor: orderStatus[status].color,
					color: 'white',
					textAlign: 'center',
					borderRadius: 20,
					display: 'inline-block',
				}}
				isTag
			/>
			<OrderDetailField label="Nama Pemesan" value={name} />
			<OrderDetailField label="Email" value={email} />
			<OrderDetailField label="Nomor Kontak" value={phone_number} />
			<OrderDetailField label="Alamat" value={address} />
			<OrderDetailField label="Kota/Pos" value={`${city}/${postal_code}`} />
			<OrderDetailField label="Tanggal Pemesanan" value={order_date} />
			<OrderDetailField
				label="Preferensi Tanggal Pengantaran"
				value={delivery_date}
			/>
			<OrderDetailField label="Total Harga" value={`Rp ${total}`} />
			<OrderDetailField label="Catatan" value={notes} />
		</Box>
	)
}

OrderDetailFields.propTypes = {
	detail: PropTypes.object,
}

export default function ProductOrderDetail() {
	const { state } = useStore()

	const [detail, setDetail] = useState('')
	const [orderItems, setOrderItems] = useState([])

	const classes = useStyles()
	const history = useHistory()

	const [openSnackbar] = useSnackbar()
	const [
		fetchDataLoading,
		showFetchDataLoading,
		hideFetchDataLoading,
	] = useLoading()

	const authHeader = {
		Authorization: `Bearer ${state.user.access_token}`,
	}

	const { id: orderId } = useParams()

	useEffect(() => {
		if (!orderId) {
			openSnackbar(
				'warning',
				'Silahkan pilih pesanan yang ingin dilihat terlebih dahulu.'
			)
			return history.push('/admin/produk/pesanan')
		}

		getOrderDetail()
	}, [])

	async function getOrderDetail() {
		showFetchDataLoading()

		try {
			const { data, errors, success } = await fetch(
				`admin/order/product/get?id=${orderId}`,
				authHeader
			)

			if (errors || !success) throw errors

			setDetail({ ...data })
			setOrderItems([...data.order_products])
		} catch (e) {
			openSnackbar('error', e.message)
		} finally {
			hideFetchDataLoading()
		}
	}

	return (
		<Container maxWidth="lg">
			<Typography component="h1" variant="h5">
				{`Detil Pesanan Produk No. ${
					!fetchDataLoading ? detail['order_ref'] : 'memuat...'
				}`}
			</Typography>
			<Container component={Paper} className={classes.ProductOrderContainer}>
				{fetchDataLoading && (
					<Box
						component="div"
						display="flex"
						alignItems="center"
						justifyContent="center"
						minHeight="50vh"
					>
						<CircularProgress />
					</Box>
				)}
				{!fetchDataLoading && detail && (
					<>
						<Grid
							container
							spacing={2}
							className={classes.OrderDetailContainer}
						>
							<Grid item xs={12} sm={4}>
								<Paper component="div" className={classes.OrderedProducts}>
									<Typography component="h3" variant="h6">
										Daftar Pesanan
									</Typography>
									<Box component="div" display="flex" flexDirection="column">
										{orderItems.length === 0 && (
											<Typography>Tidak ada item yang dipesan.</Typography>
										)}
										{orderItems.length > 0 &&
											orderItems.map(({ id, image_url, name, amount }) => (
												<OrderDetailItem
													key={id}
													image={image_url}
													name={name}
													amount={amount}
												/>
											))}
									</Box>
								</Paper>
							</Grid>
							{detail && (
								<Grid item xs={12} sm={8}>
									<OrderDetailFields detail={detail} />
								</Grid>
							)}
						</Grid>
					</>
				)}
			</Container>
		</Container>
	)
}
