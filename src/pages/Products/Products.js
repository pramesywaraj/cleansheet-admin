import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { useHistory } from 'react-router-dom'

import { post, fetch } from 'utils/api'
import uploadImage from 'utils/uploadImage'
import { useStore } from 'context/store'

import Container from '@material-ui/core/Container'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import CircularProgress from '@material-ui/core/CircularProgress'
import Box from '@material-ui/core/Box'
import ProductCard from 'components/Cards/ProductCard'
import AddProductModal from 'components/Modal/AddProductModal'

import AddIcon from '@material-ui/icons/Add'
import ListAltRoundedIcon from '@material-ui/icons/ListAltRounded'

import useLoading from 'hooks/useLoading'
import useModal from 'hooks/useModal'
import useSnackbar from 'hooks/useSnackbar'
import useLoadingSnackbar from 'hooks/useLoadingSnackbar'

const useStyles = makeStyles({
	gridRoot: {
		flexGrow: 1,
	},
	buttonMarginBottom20: {
		marginBottom: '20px',
	},
	button: {
		marginRight: 10,
	},
	progress: {
		textAlign: 'center',
	},
	centerProgress: {
		display: 'inline-block',
	},
	noProduct: {
		width: '100%',
		color: '#cfcfcf',
		textAlign: 'center',
	},
})

export default function Products() {
	const initNewProduct = {
		name: '',
		price: '',
		image_url: '',
	}
	const [productForm, setProductForm] = useState(initNewProduct)
	const [products, setProducts] = useState([])
	const [newProductImage, setNewProductImage] = useState(null)
	const [isEditProduct, setIsEditProduct] = useState(false)

	const { state } = useStore()

	const [
		fetchDataLoading,
		showFetchDataLoading,
		hideFetchDataLoading,
	] = useLoading()
	const [
		isAddProductModalOpen,
		showAddProductModal,
		hideAddProductModal,
	] = useModal()
	const [openSnackbar] = useSnackbar()
	const [openLoadingSnackbar, closeLoadingSnackbar] = useLoadingSnackbar()

	const classes = useStyles()
	const history = useHistory()

	const authHeader = {
		Authorization: `Bearer ${state.user.access_token}`,
	}

	useEffect(() => {
		getProducts()
	}, [])

	function goToOrdersList() {
		history.push('/admin/produk/pesanan')
	}

	async function getProducts() {
		showFetchDataLoading()

		try {
			const {
				data: { products },
				errors,
				success,
			} = await fetch(
				'admin/master/product/get-all?page=1&item_per_page=1000',
				authHeader
			)

			if (errors || !success) throw errors

			setProducts([...products])
		} catch (e) {
			openSnackbar('error', e.message)
		} finally {
			hideFetchDataLoading()
		}
	}

	async function postProductForm() {
		openLoadingSnackbar('Harap tunggu...')

		let productData = {
			...productForm,
		}

		let editSuccessMsg = 'Produk berhasil disunting.'
		let addSuccessMsg = 'Produk berhasil dibuat.'

		let path = 'admin/master/product/create'

		if (isEditProduct) path = 'admin/master/product/update'

		try {
			if (newProductImage) {
				const { url, errors } = await uploadImage(newProductImage)

				if (errors) throw errors

				productData['image_url'] = url
			}

			const { errors, success } = await post(path, productData, authHeader)

			if (!success || errors) throw errors

			handleHideModal()

			closeLoadingSnackbar()

			openSnackbar(
				'success',
				`${isEditProduct ? editSuccessMsg : addSuccessMsg}`
			)

			getProducts()
		} catch (e) {
			openSnackbar('error', e.message)

			closeLoadingSnackbar()
		}
	}

	async function deleteProduct(id) {
		openLoadingSnackbar('Menghapus produk...')

		try {
			const { errors, success } = await post(
				`admin/master/product/delete`,
				{ id },
				authHeader
			)

			if (errors || !success) throw errors

			closeLoadingSnackbar()

			openSnackbar('success', 'Produk berhasil dihapus')

			getProducts()
		} catch (e) {
			openSnackbar('error', e.message)

			closeLoadingSnackbar()
		}
	}

	function handleHideModal() {
		hideAddProductModal()
		setIsEditProduct(false)
		setNewProductImage(null)
		setProductForm(initNewProduct)
	}

	function handleFormValueChange(e) {
		const { name, value } = e.target
		setProductForm({
			...productForm,
			[name]: value,
		})
	}

	function handleImageChange(e) {
		const { files } = e.target
		setNewProductImage(files[0])
	}

	function handleDeleteProduct(id) {
		if (!window.confirm('Apakah Anda yakin ingin menghapus produk ini?')) return

		deleteProduct(id)
	}

	function handleAddNewProduct() {
		setIsEditProduct(false)

		setProductForm({
			...initNewProduct,
		})

		showAddProductModal()
	}

	function handleEditProduct(product) {
		setProductForm({
			...product,
		})

		setIsEditProduct(true)

		showAddProductModal()
	}

	function handleSubmit(e) {
		e.preventDefault()

		for (const [key, value] of Object.entries(productForm)) {
			if (!value && key !== 'image_url')
				return openSnackbar(
					'warning',
					`Harap isi seluruh isian yang dibutuhkan.`
				)
		}

		if (!isEditProduct && !newProductImage)
			return openSnackbar(
				'warning',
				`Harap pilih gambar untuk merepresentasikan produk.`
			)

		postProductForm()
	}

	return (
		<>
			<Container>
				<Box
					display="flex"
					flexDirection="row-reverse"
					className={classes.buttonMarginBottom20}
				>
					<Button
						variant="contained"
						color="primary"
						className={classes.button}
						onClick={handleAddNewProduct}
					>
						<AddIcon />
						Tambahkan Produk Baru
					</Button>
					<Button
						variant="contained"
						color="primary"
						className={classes.button}
						onClick={goToOrdersList}
					>
						<ListAltRoundedIcon />
						Lihat Pesanan Produk
					</Button>
				</Box>
				{!fetchDataLoading && products.length === 0 && (
					<Box
						component="div"
						display="flex"
						alignItems="center"
						justifyContent="center"
						minHeight="50vh"
					>
						<h3>Tidak ada Produk yang dapat ditampilkan.</h3>
					</Box>
				)}

				{fetchDataLoading && (
					<Box
						component="div"
						display="flex"
						alignItems="center"
						justifyContent="center"
						minHeight="50vh"
					>
						<CircularProgress />
					</Box>
				)}
			</Container>

			{!fetchDataLoading && products.length > 0 && (
				<Grid container spacing={3} className={classes.gridRoot}>
					{products.map((product, index) => (
						<Grid item xs={4} key={index}>
							<ProductCard
								productData={product}
								editHandler={handleEditProduct}
								deleteHandler={handleDeleteProduct}
							/>
						</Grid>
					))}
				</Grid>
			)}

			<AddProductModal
				onEdit={isEditProduct}
				open={isAddProductModalOpen}
				handleClose={handleHideModal}
				handleChangeValue={handleFormValueChange}
				handleImageChange={handleImageChange}
				handleSubmit={handleSubmit}
				newProduct={productForm}
				imageName={newProductImage ? newProductImage.name : null}
			/>
		</>
	)
}
