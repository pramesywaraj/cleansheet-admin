import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'

import { post, fetch } from 'utils/api'
import uploadImage from 'utils/uploadImage'
import { useStore } from 'context/store'

import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'
import Box from '@material-ui/core/Box'
import TableCell from '@material-ui/core/TableCell'
import TableRow from '@material-ui/core/TableRow'
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton'
import AddAccountModal from 'components/Modal/AddAccountModal'
import MaterialTableLayout from 'components/Table/Table'

import DeleteIcon from '@material-ui/icons/Delete'
import AddIcon from '@material-ui/icons/Add'

import useLoading from 'hooks/useLoading'
import useModal from 'hooks/useModal'
import useSnackbar from 'hooks/useSnackbar'
import useLoadingSnackbar from 'hooks/useLoadingSnackbar'

const useStyles = makeStyles({
	table: {
		minWidth: 650,
	},
	tableContainer: {
		padding: '2% 0',
	},
	textBoxContainer: {
		margin: '1% 0',
		width: '70%',
	},
	contactInfoContainer: {
		paddingLeft: 20,
		width: '40%',
	},
	inputContact: {
		flex: 2,
		marginRight: 10,
	},
	deleteButton: {
		color: '#e30926',
	},
	phoneNumber: {
		marginLeft: 20,
	},
	LoadingContainer: {
		display: 'flex',
		width: '100%',
		minHeight: '20vh',
		justifyContent: 'center',
		alignItems: 'center',
	},
	RowCursor: {
		cursor: 'pointer',
	},
})

export default function ContactAccount() {
	const initialNewAccount = {
		code: '',
		name: '',
		description: '',
		account_number: '',
		account_name: '',
		image_url: '',
	}

	const { state } = useStore()
	const [accounts, setAccounts] = useState([])
	const [accountForm, setAccountForm] = useState(initialNewAccount)
	const [accountImage, setAccountImage] = useState(null)
	const [isEdit, setIsEdit] = useState(false)

	const [
		fetchDataLoading,
		showFetchDataLoading,
		hideFetchDataLoading,
	] = useLoading()
	const [
		isAddAccountModal,
		showAddAcountModal,
		hideAddAccountModal,
	] = useModal()
	const classes = useStyles()
	const [openSnackbar] = useSnackbar()
	const [openLoadingSnackbar, closeLoadingSnackbar] = useLoadingSnackbar()

	const authHeader = {
		Authorization: `Bearer ${state.user.access_token}`,
	}

	async function getAccounts() {
		showFetchDataLoading()

		try {
			const { data, errors, success } = await fetch(
				'admin/master/payment?page=1&item_per_page=1',
				authHeader
			)

			if (errors || !success) throw errors

			setAccounts([...data])
		} catch (e) {
			openSnackbar('error', e.message)
		} finally {
			hideFetchDataLoading()
		}
	}

	async function deleteAccount(id) {
		openLoadingSnackbar('Menghapus akun...')

		try {
			const { errors, success } = await post(
				`admin/master/payment/delete`,
				{ id },
				authHeader
			)

			if (errors || !success) throw errors

			closeLoadingSnackbar()

			openSnackbar('success', 'Akun berhasil dihapus')

			getAccounts()
		} catch (e) {
			openSnackbar('error', e.message)

			closeLoadingSnackbar()
		}
	}

	async function postAnAccount() {
		openLoadingSnackbar('Harap tunggu...')

		let accountData = {
			...accountForm,
		}

		let path = 'admin/master/payment/create'
		let editSuccessMsg = 'Akun berhasil disunting.'
		let addSuccessMsg = 'Akun berhasil dibuat.'

		if (isEdit) path = 'admin/master/payment/update'

		try {
			if (accountImage) {
				const { url, errors } = await uploadImage(accountImage)

				if (errors) throw errors

				accountData['image_url'] = url
			}

			const { errors, success } = await post(path, accountData, authHeader)

			if (!success || errors) throw errors

			handleHideModal()

			closeLoadingSnackbar()

			openSnackbar('success', `${isEdit ? editSuccessMsg : addSuccessMsg}`)

			getAccounts()
		} catch (e) {
			openSnackbar('error', e.message)

			closeLoadingSnackbar()
		}
	}

	useEffect(() => {
		getAccounts()
	}, [])

	function handleChangeValue(e) {
		setAccountForm({
			...accountForm,
			[e.target.name]: e.target.value,
		})
	}

	function handleAccountImageChange(e) {
		const { files } = e.target
		setAccountImage(files[0])
	}

	function handleHideModal() {
		setAccountForm(initialNewAccount)
		setAccountImage(null)

		setIsEdit(false)

		hideAddAccountModal()
	}

	function handleSubmit(e) {
		e.preventDefault()

		for (const [key, value] of Object.entries(accountForm)) {
			if (!value && key !== 'image_url')
				return openSnackbar(
					'warning',
					`Harap isi seluruh isian yang dibutuhkan.`
				)
		}

		if (!isEdit && !accountImage)
			return openSnackbar(
				'warning',
				`Harap pilih gambar untuk merepresentasikan akun.`
			)

		postAnAccount()
	}

	function handleDeleteAccount(e, id) {
		e.stopPropagation()

		if (!window.confirm('Apakah Anda yakin ingin menghapus akun ini?')) return

		deleteAccount(id)
	}

	function handleAddNewAccount() {
		setIsEdit(false)

		setAccountForm({
			...initialNewAccount,
		})

		showAddAcountModal()
	}

	function handleSelectAccount(account) {
		setAccountForm({
			...account,
		})

		setIsEdit(true)

		showAddAcountModal()
	}

	return (
		<>
			<Container maxWidth="lg">
				<Typography component="h1" variant="h5">
					Akun Rekening Pembayaran
				</Typography>
				<Box display="flex" justifyContent="flex-end">
					<Button
						variant="contained"
						color="primary"
						onClick={handleAddNewAccount}
					>
						<AddIcon />
						Tambahkan Akun Baru
					</Button>
				</Box>
				<MaterialTableLayout
					head={
						<TableRow>
							<TableCell>Kode Akun</TableCell>
							<TableCell>Nama Akun</TableCell>
							<TableCell>Deskripsi</TableCell>
							<TableCell>Pemilik Akun</TableCell>
							<TableCell align="right">Nomor Akun</TableCell>
							<TableCell></TableCell>
						</TableRow>
					}
					body={accounts.map((account) => (
						<TableRow
							className={classes.RowCursor}
							hover
							key={account.code}
							onClick={(e) => handleSelectAccount(account)}
						>
							<TableCell component="th" scope="row">
								{account.code}
							</TableCell>
							<TableCell>{account.name}</TableCell>
							<TableCell>{account.description}</TableCell>
							<TableCell>{account.account_name}</TableCell>
							<TableCell align="right">{account.account_number}</TableCell>
							<TableCell align="right">
								<IconButton
									aria-label="delete"
									className={classes.margin}
									onClick={(e) => handleDeleteAccount(e, account.id)}
								>
									<DeleteIcon className={classes.deleteButton} />
								</IconButton>
							</TableCell>
						</TableRow>
					))}
					isFetching={fetchDataLoading}
					label="account-table"
					isNoData={accounts.length === 0}
				></MaterialTableLayout>
			</Container>
			<AddAccountModal
				onEdit={isEdit}
				open={isAddAccountModal}
				handleClose={handleHideModal}
				handleChangeValue={handleChangeValue}
				handleAccountImageChange={handleAccountImageChange}
				handleSubmit={handleSubmit}
				values={accountForm}
				imageName={accountImage ? accountImage.name : null}
			/>
		</>
	)
}
