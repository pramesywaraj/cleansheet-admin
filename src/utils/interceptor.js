import { useEffect, useState } from 'react'
import axios from 'axios'

import { useStore } from 'context/store'
import { post } from 'utils/api'

import { TOKEN_REFRESHED } from 'constants/command'

export default function Interceptors() {
	const [errorInterceptor, setErrorInterceptor] = useState(undefined)
	const { state, dispatch } = useStore()

	function removeErrorInterceptor() {
		axios.interceptors.response.eject(errorInterceptor)
		setErrorInterceptor(undefined)
	}

	function addTokenInvalidInterceptor() {
		const interceptor = axios.interceptors.response.use((mainResponse) => {
			const { data, config } = mainResponse

			if (data.code === 403 && data.errors.message === 'Token tidak valid!') {
				axios.interceptors.response.eject(interceptor)

				return post('auth/token/refresh', {
					access_token: state.user.access_token,
					refresh_token: state.user.refresh_token,
				})
					.then((res) => {
						const { data } = res
						const { access_token } = data

						dispatch({
							type: TOKEN_REFRESHED,
							data: { ...data },
						})

						config.headers.Authorization = `Bearer ${access_token}`

						return axios(config)
					})
					.catch((error) => {
						return Promise.reject(error)
					})
					.finally(addTokenInvalidInterceptor)
			}

			return mainResponse
		})
		setErrorInterceptor(interceptor)
	}

	useEffect(() => {
		addTokenInvalidInterceptor()

		return () => {
			removeErrorInterceptor()
		}
	}, [])

	return null
}
