import { post } from 'utils/api'

export default async function uploadImage(image) {
	const imageUploadOption = {
		'Content-Type': 'multipart/form-data',
	}

	let imageData = new FormData()
	imageData.append('image', image)

	try {
		const {
			data: { image_url },
			errors,
		} = await post(`image/upload`, imageData, imageUploadOption)

		if (!image_url) throw errors

		return { url: image_url, errors: '' }
	} catch (e) {
		return { url: '', errors: e }
	}
}
