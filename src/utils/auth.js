import { useEffect } from 'react'
import { useStore } from 'context/store'

import { LOGIN_SUCCESS } from 'constants/command'

export function setUserData(data) {
	return localStorage.setItem('CS_ADMIN_USER', JSON.stringify(data))
}

export function getUserData() {
	return JSON.parse(localStorage.getItem('CS_ADMIN_USER'))
}

export function removeUserData() {
	return localStorage.removeItem('CS_ADMIN_USER')
}

export default function AuthCheck() {
	const { dispatch } = useStore()

	function checkUserData() {
		const user = getUserData()

		if (!user) return

		dispatch({
			type: LOGIN_SUCCESS,
			data: {
				...user,
			},
		})
	}

	useEffect(() => {
		checkUserData()

		return () => {}
	}, [])

	return null
}
