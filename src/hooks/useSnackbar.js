import { useStore } from 'context/store'

import { SNACKBAR_OPEN, SNACKBAR_CLOSE } from 'constants/command'

export default function useSnackbar() {
	const { dispatch } = useStore()

	function openSnackbar(type, message) {
		dispatch({
			type: SNACKBAR_OPEN,
			data: {
				type,
				message,
			},
		})
	}

	function closeSnackbar() {
		dispatch({
			type: SNACKBAR_CLOSE,
		})
	}

	return [openSnackbar, closeSnackbar]
}
