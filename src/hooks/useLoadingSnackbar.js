import { useStore } from 'context/store'

import {
	SNACKBAR_LOADING_OPEN,
	SNACKBAR_LOADING_CLOSE,
} from 'constants/command'

export default function useLoadingSnackbar() {
	const { dispatch } = useStore()

	function openLoadingSnackbar(message) {
		dispatch({
			type: SNACKBAR_LOADING_OPEN,
			data: {
				message,
			},
		})
	}

	function closeLoadingSnackbar() {
		dispatch({
			type: SNACKBAR_LOADING_CLOSE,
		})
	}

	return [openLoadingSnackbar, closeLoadingSnackbar]
}
